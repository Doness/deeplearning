/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.latikai.util;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

/**
 *
 * @author sdonn
 */
public class Graphing {
    
    public static class OutputComboChart extends ApplicationFrame {

        public OutputComboChart(double[] outputs, double[] gts, double[] memories, int start, int length) {
            super("Output history");
            JFreeChart lineChart = ChartFactory.createXYLineChart("responses during training", "training epoch", "response", createDataset(outputs, gts, memories, start, length));

            ChartPanel chartPanel = new ChartPanel(lineChart);
            chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
            setContentPane(chartPanel);
        }

        public OutputComboChart(double[] outputs, double[] gts, double[] memories) {
            this(outputs, gts, memories, 0, gts.length);
        }

        private DefaultXYDataset createDataset(double[] outputs, double[] gts, double[] memories, int start, int length) {
            DefaultXYDataset dataset = new DefaultXYDataset();
            double[][] series = new double[2][length];
            for (int i = 0; i < length; i++) {
                series[0][i] = i;
                double val = outputs[i + start];
                if(val == Double.NaN)
                    val = 0.0;
                series[1][i] = val;
            }
            dataset.addSeries("Output", series);
            series = new double[2][length];
            for (int i = 0; i < length; i++) {
                series[0][i] = i;
                double val = gts[i + start];
                if(val == Double.NaN)
                    val = 0.0;
                series[1][i] = val;
            }
            dataset.addSeries("Groundtruth", series);
            series = new double[2][length];
            for (int i = 0; i < length; i++) {
                series[0][i] = i;
                double val = memories[i + start];
                if(val == Double.NaN)
                    val = 0.0;
                series[1][i] = val;
            }
            dataset.addSeries("Memory", series);
            return dataset;
        }
    }

    public static class OutputChart extends ApplicationFrame {

        public OutputChart(String name, double[] values, int start, int length) {
            super("Output history");
            JFreeChart lineChart = ChartFactory.createXYLineChart(name, "training epoch", "response", createDataset(values, name, start, length));

            ChartPanel chartPanel = new ChartPanel(lineChart);
            chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
            setContentPane(chartPanel);
        }

        public OutputChart(String name, double[] values) {
            this(name, values, 0, values.length);
        }

        private DefaultXYDataset createDataset(double[] values, String name, int start, int length) {
            DefaultXYDataset dataset = new DefaultXYDataset();
            double[][] series = new double[2][length];
            for (int i = 0; i < length; i++) {
                series[0][i] = i;
                double val = values[i + start];
                if((val == Double.NaN) || (val==Double.NEGATIVE_INFINITY) || (val==Double.POSITIVE_INFINITY))
                    val = 0.0;
                series[1][i] = val;
            }
            dataset.addSeries(name, series);
            return dataset;
        }
    }
    public static void plot(String name, double[] values)
    {
        plot(name,values,0,values.length);
    }
    
    public static void plot(String name, double[] values, int start, int length)
    {
        OutputChart chart = new OutputChart(name, values,start,length);
        chart.pack();
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
    }
}
