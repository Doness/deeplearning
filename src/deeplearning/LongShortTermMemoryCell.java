/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deeplearning;

import org.latikai.util.Graphing;

/**
 *
 * @author sdonn
 */
public class LongShortTermMemoryCell extends Neuron {

    //general variables
    int channels_in, memory_size, channels_out;
    double[] memory;
    double learning_rate = 1e-2;

    //forward processing variables
    double[][] Wxs, Wxi, Wci, Wcs, Wxo, Wco, Wxf, Wcf;
    //backpropagation direction buffers
    double[][] dWxs, dWxi, dWci, dWcs, dWxo, dWco, dWxf, dWcf;

    public LongShortTermMemoryCell(int channels_in, int memory_size, int channels_out) {
        this.channels_in = channels_in;
        this.memory_size = memory_size;
        this.channels_out = channels_out;

        memory = init_field(memory_size);

        Wxs = init_filter(channels_in, memory_size);
        Wxi = init_filter(channels_in, memory_size);
        Wci = init_filter(memory_size, memory_size);
        Wxf = init_filter(channels_in, memory_size);
        Wcf = init_filter(memory_size, memory_size);
        Wxo = init_filter(channels_in, channels_out);
        Wco = init_filter(memory_size, channels_out);
        Wcs = init_filter(memory_size, channels_out);

        dWxs = zero_buffer(channels_in, memory_size);
        dWxi = zero_buffer(channels_in, memory_size);
        dWci = zero_buffer(memory_size, memory_size);
        dWxf = zero_buffer(channels_in, memory_size);
        dWcf = zero_buffer(memory_size, memory_size);
        dWxo = zero_buffer(channels_in, channels_out);
        dWco = zero_buffer(memory_size, channels_out);
        dWcs = zero_buffer(memory_size, channels_out);
    }

    public void reset_memory() {
        memory = init_field(memory_size);
    }
    
    public boolean testnan() {
        return isnan(Wxs)
                || isnan(Wxi)
                || isnan(Wci)
                || isnan(Wcs)
                || isnan(Wxo)
                || isnan(Wco)
                || isnan(Wxf)
                || isnan(Wcf);
    }

    public double[] process(double[] input) {
        double[] ig = add(apply_filter(input, Wxi), apply_filter(memory, Wci));
        double[] xs = activation_function(apply_filter(input, Wxs));
        double[] f = add(apply_filter(input, Wxf), apply_filter(memory, Wcf));

        double[] o = add(apply_filter(input, Wxo), apply_filter(memory, Wco));
        double[] cs = activation_function(apply_filter(memory, Wcs));
        double[] h = hadamard(o, cs);

        //update the memory
        memory = add(hadamard(ig, xs), hadamard(f, memory));
        //return the output
        return h;
    }

    public double[] backpropagate_output_error(double[] derror_doutput, double[] memory, double[] input) {
        //forward run
        double[] o = add(apply_filter(input, Wxo), apply_filter(memory, Wco));
        double[] cs = activation_function(apply_filter(memory, Wcs));
        double[] dcs = dactivation_function(apply_filter(memory, Wcs));

        //backpropagation
        double[] dPhidh = derror_doutput;

        {
            double[][] dPhidWcs = new double[memory_size][channels_out];
            for (int i = 0; i < channels_out; i++) {
                for (int k = 0; k < memory_size; k++) {
                    dPhidWcs[k][i] = dPhidh[i] * o[i] * dcs[i] * memory[k];
                }
            }
            dWcs = add(dWcs, dPhidWcs);
        }

        double[] dPhido = hadamard(dPhidh, cs);
        double[][] dhdc  = new double[memory_size][channels_out];
        for (int k = 0; k < memory_size; k++) {
            for (int i = 0; i < channels_out; i++) {
                dhdc[k][i] = o[i]*dcs[i]*Wcs[k][i] + cs[i]*Wco[k][i];
            }
        }
        
        double[] dPhidc = apply_filter_transposed(dPhidh, dhdc);
        
        {
            double[][] dPhidWxo = new double[channels_in][channels_out];
            for (int j = 0; j < channels_in; j++) {
                for (int i = 0; i < channels_out; i++) {
                    dPhidWxo[j][i] = dPhido[i] * input[j];
                }
            }
            dWxo = add(dWxo, dPhidWxo);
        }
        {
            double[][] dPhidWco = new double[memory_size][channels_out];
            for (int k = 0; k < memory_size; k++) {
                for (int i = 0; i < channels_out; i++) {
                    dPhidWco[k][i] = dPhido[i] * memory[k];
                }
            }
            dWco = add(dWco, dPhidWco);
        }

        return dPhidc;
    }

    public double[] backpropagate_memory_error(double[] derror_dmemory, double[] memory, double[] input) {
        //forward run
        double[] ig = add(apply_filter(input, Wxi), apply_filter(memory, Wci));
        double[] xs = activation_function(apply_filter(input, Wxs));
        double[] dxs = dactivation_function(apply_filter(input, Wxs));

        double[] dPhidcout = derror_dmemory;

        {
            double[][] dPhidWxs = new double[channels_in][memory_size];
            for (int j = 0; j < channels_in; j++) {
                for (int k = 0; k < memory_size; k++) {
                    dPhidWxs[j][k] = dPhidcout[k] * ig[k] * dxs[k] * input[j];
                }
            }
            dWxs = add(dWxs, dPhidWxs);
        }
        {
            double[][] dPhidWxi = new double[channels_in][memory_size];
            for (int k = 0; k < memory_size; k++) {
                for (int j = 0; j < channels_in; j++) {
                    dPhidWxi[j][k] = dPhidcout[k] * xs[k] * input[j];
                }
            }
            dWxi = add(dWxi, dPhidWxi);
        }
        {
            double[][] dPhidWci = new double[memory_size][memory_size];
            for (int k = 0; k < memory_size; k++) {
                for (int l = 0; l < memory_size; l++) {
                    dPhidWci[l][k] = dPhidcout[k] * xs[k] * memory[l];
                }
            }
            dWci = add(dWci, dPhidWci);
        }
        {
            double[][] dPhidWxf = new double[channels_in][memory_size];
            for (int k = 0; k < memory_size; k++) {
                for (int j = 0; j < channels_in; j++) {
                    dPhidWxf[j][k] = dPhidcout[k] * memory[k] * input[j];
                }
            }
            dWxf = add(dWxf, dPhidWxf);
        }

        {
            double[][] dPhidWcf = new double[memory_size][memory_size];
            for (int k = 0; k < memory_size; k++) {
                for (int l = 0; l < memory_size; l++) {
                    dPhidWcf[l][k] = dPhidcout[k] * memory[k] * memory[l];
                }
            }
            dWcf = add(dWcf, dPhidWcf);
        }

        double[] dPhidcin = new double[memory_size];
        double[][] dcoutdcin = new double[memory_size][memory_size];
        for (int k = 0; k < memory_size; k++) {
            for (int l = 0; l < memory_size; l++) {
                dcoutdcin[k][l] = Wcf[l][k] * memory[k] + Wci[l][k] * xs[k];
            }
            for (int j = 0; j < input.length; j++) {
                dcoutdcin[k][k] += Wxf[j][k] * input[j];
            }
            for (int l = 0; l < memory_size; l++) {
                dcoutdcin[k][k] += Wcf[l][k] * memory[l];
            }
        }
        for (int l = 0; l < dPhidcin.length; l++) {
            dPhidcin[l] = 0.0;
            for (int k = 0; k < dPhidcout.length; k++) {
                dPhidcin[l] += dPhidcout[k] * dcoutdcin[k][l];
            }
        }
        return dPhidcin;
    }

    public void print_improving_step_size() {
        this.print_improving_step_size(1.0);
    }

    public void print_improving_step_size(double adjust_factor) {
        double norm = 0.0;
        norm += Math.pow(norm(dWxs), 2);
        norm += Math.pow(norm(dWxi), 2);
        norm += Math.pow(norm(dWci), 2);
        norm += Math.pow(norm(dWxf), 2);
        norm += Math.pow(norm(dWcf), 2);
        norm += Math.pow(norm(dWxo), 2);
        norm += Math.pow(norm(dWco), 2);
        norm += Math.pow(norm(dWcs), 2);
        System.out.println("Improving step size: " + Math.sqrt(norm) * learning_rate * adjust_factor);
    }

    public void take_improving_step(double adjust_factor) {
        if (learning_rate > 0) {
            //take the improving step
            Wxs = sub(Wxs, mul(dWxs, learning_rate * adjust_factor));
            Wxi = sub(Wxi, mul(dWxi, learning_rate * adjust_factor));
            Wci = sub(Wci, mul(dWci, learning_rate * adjust_factor));
            Wxf = sub(Wxf, mul(dWxf, learning_rate * adjust_factor));
            Wcf = sub(Wcf, mul(dWcf, learning_rate * adjust_factor));
            Wxo = sub(Wxo, mul(dWxo, learning_rate * adjust_factor));
            Wco = sub(Wco, mul(dWco, learning_rate * adjust_factor));
            Wcs = sub(Wcs, mul(dWcs, learning_rate * adjust_factor));
        }
        //zero the buffers
        dWxs = zero_buffer(channels_in, memory_size);
        dWxi = zero_buffer(channels_in, memory_size);
        dWci = zero_buffer(memory_size, memory_size);
        dWxf = zero_buffer(channels_in, memory_size);
        dWcf = zero_buffer(memory_size, memory_size);
        dWxo = zero_buffer(channels_in, channels_out);
        dWco = zero_buffer(memory_size, channels_out);
        dWcs = zero_buffer(memory_size, channels_out);
    }

    public void take_improving_step() {
        this.take_improving_step(1.0);
    }

    public void perform_training(double[][] input, double[][] groundtruth) {
        this.perform_training(input, groundtruth, 10);//totally arbitrarily chosen value
    }
    
    public void handle_training_set(double[][][] inputs, double[][][] groundtruths, int shortterm) {
        for (int i = 0; i < groundtruths.length; i++) {
            this.memory = init_field(memory_size);
            this.perform_training(inputs[i],groundtruths[i],shortterm,false);
        }
    }

    public void perform_training(double[][] inputs, double[][] groundtruths, int shortterm) {
        this.perform_training(inputs, groundtruths, shortterm, true);
    }
    
    public void perform_training(double[][] inputs, double[][] groundtruths, int shortterm, boolean verbose) {
        int input_length = inputs.length;
        double[] tracked_memory = this.memory.clone();//contains the memory at the start of the BPTT iteration
        double[][] step_memories = new double[shortterm][memory_size];//contains the memory at the start of a given short-term iteration
        double[] errors = new double[input_length];
        for (int i = 0; i < input_length - shortterm; i++) {
            //forward propagation: keep track of all of the memories as we go
            double[] output = new double[channels_out];
            for (int t = 0; t < shortterm; t++) {
                step_memories[t] = this.memory.clone();
                output = this.process(inputs[i + t]);
            }
            //get the output error
            double[] errorvec = Neuron.sub(output, groundtruths[i + shortterm - 1]);
            errors[i] = Neuron.sum(Neuron.mul(errorvec, errorvec));
            double[] derror_doutput = Neuron.mul(Neuron.sub(output, groundtruths[i + shortterm - 1]), 2.0);
            //backpropagate the error, first the final iteration
            double[] derror_dmemory = this.backpropagate_output_error(derror_doutput, step_memories[shortterm - 1], inputs[i + shortterm - 1]);
            //backpropagate the derivative of the error wrt the memory
            for (int t = shortterm - 2; t >= 0; t--) {
                derror_dmemory = this.backpropagate_memory_error(derror_dmemory, step_memories[t], inputs[i + t]);
            }
            //take the improving step
            this.take_improving_step(1.0 / shortterm);
            //take a single step forward, and repeat
            this.memory = tracked_memory.clone();
            this.process(inputs[i]);
            tracked_memory = this.memory.clone();
        }

        if(verbose)
            Graphing.plot("Errors", errors, 0, input_length - shortterm);
    }
}
