package deeplearning;

public class DeepLearning {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (false) {//Test 1 - speed
            //create a LongShortTermMemoryCell - prepared for 39 input channels (compressed mel cepstra input)
            //and should output roughly 60 classifications (number of phonemes)
            LongShortTermMemoryCell cell = new LongShortTermMemoryCell(39, 128, 60);

            double[] demo_in = Neuron.init_field(39);
            long start = System.currentTimeMillis();
            for (int i = 0; i < 100; i++) {
                cell.process(demo_in);
            }
            long stop = System.currentTimeMillis();
            System.out.println("Average time to process a single AudioFeatureVector: " + ((double) (stop - start)) / 100 + " ms.");
        }
        if (false) {//Test 2 - training
            //create a LongShortTermMemoryCell which is just supposed to return the value, one time step later
            //this simple scenario can be easily achieved with manually chosen values
            LongShortTermMemoryCell cell = new LongShortTermMemoryCell(2, 1, 1);

            //the values that should be chosen for perfect (after the first training sample) performance:
//            cell.Wcs[0][0] = 1.0;
//            cell.Wxf[0][0] = 0.0;
//            cell.Wxf[1][0] = 0.0;
//            cell.Wcf[0][0] = 0.0;
//            cell.Wxi[0][0] = 0.0;
//            cell.Wxi[1][0] = 1.0;
//            cell.Wci[0][0] = 0.0;
//            cell.Wxo[0][0] = 0.0;
//            cell.Wxo[1][0] = 1.0;
//            cell.Wco[0][0] = 0.0;
//            cell.Wxs[0][0] = 1.0;
//            cell.Wxs[1][0] = 0.0;
            int training_length = 10000;
            double[][] inputs = new double[training_length][2];
            double[][] groundtruths = new double[training_length][1];

            for (int i = 0; i < training_length; i++) {
                inputs[i][0] = (double) (Math.floor(Math.random() * 2));
                inputs[i][1] = 1.0;
                groundtruths[i][0] = (i > 0) ? inputs[i - 1][0] : 0.0;
            }
            long start = System.currentTimeMillis();
            cell.perform_training(inputs, groundtruths, 5);
            long stop = System.currentTimeMillis();
            System.out.println("Time for processing " + training_length + " backpropagations: " + ((double) (stop - start)) + " ms.");
        }
        if (true) {//Test 5 - binary addition
            //create a LongShortTermMemoryCell which is just supposed to return the sum of two numbers
            LongShortTermMemoryCell cell = new LongShortTermMemoryCell(2, 16, 1);
            
            int set_size = 10000;
            int number_length = 16;
            double[][][] inputs = new double[set_size][number_length][2];
            double[][][] groundtruths = new double[set_size][number_length][1];
            for (int i = 0; i < set_size; i++) {
                int sum = 0;
                for (int j = 0; j < number_length; j++) {
                    long a = Math.round(Math.random());
                    long b = Math.round(Math.random());
                    long c = a+b+sum;
                    sum = c>=2?1:0;
                    c = c % 2;
                    inputs[i][j][0] = a;
                    inputs[i][j][1] = b;
                    groundtruths[i][j][0] = c;
                }
            }
            
            long start = System.currentTimeMillis();
            cell.handle_training_set(inputs, groundtruths, 15);
            long stop = System.currentTimeMillis();
            cell.learning_rate = 0.001;
            System.out.println("Time for processing " + set_size + " sets of " + number_length + "-digit binary numbers: " + ((double) (stop - start)) + " ms.");
            
            //check the resulting machine
            String actual_output = "";
            System.out.println("Input: ");
            System.out.print("\t");
            for (int i = 0; i < number_length; i++) {
                System.out.print("" + (int) inputs[0][i][0]);
            }System.out.println("");
            System.out.print("\t");
            for (int i = 0; i < number_length; i++) {
                System.out.print("" + (int) inputs[0][i][1]);
            }System.out.println("");
            System.out.println("Output: ");
            System.out.print("\t");
            cell.reset_memory();
            for (int i = 0; i < number_length; i++) {
                double out = cell.process(inputs[0][i])[0];
                System.out.print(""+Math.round(out));
                actual_output += " " + out;
            }System.out.println("");
            System.out.println("Groundtruth: ");
            System.out.print("\t");
            for (int i = 0; i < number_length; i++) {
                System.out.print(""+ (int) groundtruths[0][i][0]);
            }System.out.println("");
            
            System.out.println("Actual output: "+actual_output);
        }
    }
}
