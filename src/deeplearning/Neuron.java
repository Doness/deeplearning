/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deeplearning;

/**
 *
 * @author sdonn
 */
public abstract class Neuron {

    static final double[] init_field(int D) {
        double[] result = new double[D];
        for (int i = 0; i < result.length; i++) {
            result[i] = 0.0;
        }
        return result;
    }

    static final double[][] init_filter(int M, int N) {
        double[][] result = new double[M][N];
        for (int m = 0; m < M; m++) {
            for (int n = 0; n < N; n++) {
                result[m][n] = Math.random()/M/M;
            }
        }
        return result;
    }

    static final boolean isnan(double[][] a) {
        boolean isnan = false;
        for (double[] dv : a) {
            for (double d : dv) {
                isnan = isnan || (d == Double.NaN) || (d == Double.POSITIVE_INFINITY);
            }
        }
        return isnan;
    }

    static final double[][] zero_buffer(int M, int N) {
        double[][] result = new double[M][N];
        for (int m = 0; m < M; m++) {
            for (int n = 0; n < N; n++) {
                result[m][n] = 0.0;
            }
        }
        return result;
    }

    static final double[] apply_filter(double[] input, double[][] filter) {
        int c_in = input.length;
        int c_out = filter[0].length;
        double[] result = new double[c_out];
        for (int co = 0; co < c_out; co++) {
            result[co] = 0.0;
            for (int ci = 0; ci < c_in; ci++) {
                result[co] += input[ci] * filter[ci][co];
            }
        }
        return result;
    }
    
    static final double[] apply_filter_transposed(double[] input, double[][] filter) {
        int c_in = input.length;
        int c_out = filter.length;
        double[] result = new double[c_out];
        for (int co = 0; co < c_out; co++) {
            result[co] = 0.0;
            for (int ci = 0; ci < c_in; ci++) {
                result[co] += input[ci] * filter[co][ci];
            }
        }
        return result;
    }

    static final double[] hadamard(double[] a, double[] b) {
        double[] result = new double[a.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = a[i] * b[i];
        }
        return result;
    }

    static final double[] add(double[] a, double[] b) {
        double[] result = new double[a.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = a[i] + b[i];
        }
        return result;
    }

    static final double[] mul(double[] a, double b) {
        double[] result = new double[a.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = a[i] * b;
        }
        return result;
    }

    static final double[][] mul(double[][] a, double b) {
        double[][] result = new double[a.length][a[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = a[i][j] * b;
            }
        }
        return result;
    }

    static final double[] mul(double[] a, double[] b) {
        double[] result = new double[a.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = a[i] * b[i];
        }
        return result;
    }

    static final double[] sub(double[] a, double[] b) {
        double[] result = new double[a.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = a[i] - b[i];
        }
        return result;
    }

    static final double[][] add(double[][] a, double[][] b) {
        double[][] result = new double[a.length][a[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = a[i][j] + b[i][j];
            }
        }
        return result;
    }

    static final double sum(double[][] a) {
        double result = 0.0;
        for (double[] a2 : a) {
            for (double a1 : a2) {
                result += a1;
            }
        }
        return result;
    }

    static final double sum(double[] a) {
        double result = 0.0;
        for (double a1 : a) {
            result += a1;
        }
        return result;
    }

    static final double norm(double[][] a) {
        double result = 0.0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                result += a[i][j] * a[i][j];
            }
        }
        return Math.sqrt(result);
    }

    static final double norm(double[] a) {
        double result = 0.0;
        for (double a1 : a) {
            result += a1 * a1;
        }
        return Math.sqrt(result);
    }

    static final double[][] div(double[][] a, double b) {
        double[][] result = new double[a.length][a[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = a[i][j] / b;
            }
        }
        return result;
    }

    static final double[] div(double[] a, double b) {
        double[] result = new double[a.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = a[i] / b;
        }
        return result;
    }

    static final double[][] sub(double[][] a, double[][] b) {
        double[][] result = new double[a.length][a[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = a[i][j] - b[i][j];
            }
        }
        return result;
    }

    static final double[] cat(double[] a, double[] b) {
        double[] result = new double[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    static double[] activation_function(double[] in) {
        double[] result = new double[in.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = activation_function(in[i]);
        }
        return result;
    }

    static double[] dactivation_function(double[] in) {
        double[] result = new double[in.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = dactivation_function(in[i]);
        }
        return result;
    }

    //sigmoids
//    static double activation_function(double in) {
//        return 1 / (1 + Math.exp(-in));
//    }
//    static double dactivation_function(double in) {
//        return activation_function(in) * (1 - activation_function(in));
//    }
//    //leaky ReLU
//    static double activation_function(double in) {
//        return (in > 0) ? in : 0.01 * in;
//    }
//
//    static double dactivation_function(double in) {
//        return (in > 0) ? 1.0 : -0.01;
//    }
    //no activation function
    static double activation_function(double in) {
        return in;
    }

    static double dactivation_function(double in) {
        return 1.0;
    }
}
